//
//  ViewController.m
//  noidle
//
//  Created by Nick Galasso on 5/14/15.
//  Copyright (c) 2015 Nick Galasso. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

@end

//
//  main.m
//  noidle
//
//  Created by Nick Galasso on 5/14/15.
//  Copyright (c) 2015 Nick Galasso. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}

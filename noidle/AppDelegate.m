//
//  AppDelegate.m
//  noidle
//
//  Created by Nick Galasso on 5/14/15.
//  Copyright (c) 2015 Nick Galasso. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application

    [self startTimer];
    
}

-(void)startTimer{
    
    self.timer = [NSTimer timerWithTimeInterval:5
                                         target:self
                                       selector:@selector(focusHipchat)
                                       userInfo:nil
                                        repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.timer
                              forMode:NSDefaultRunLoopMode];

}

-(void)focusHipchat{

    NSLog(@"giving hipchat focus");
    
    NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
    
    [workspace launchApplication:@"HipChat"];
    
}

@end
